package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        List<Project> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean isFoundById(final String id) {
        return findById(id) != null;
    }

    @Override
    public void add(final Project project) {
        list.add(project);
    }

    @Override
    public void remove(final Project project) {
        list.remove(project);
    }

    @Override
    public Project update(final String id, final String name, final String description) {
        final Project project = findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Project findById(final String id) {
        for (Project project : list) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        this.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final int index) {
        return list.remove(index);
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        this.remove(project);
        return project;
    }

    @Override
    public Project updateStatus(final String id, final Status status) {
        final Project project = findById(id);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) project.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public String getId(final String name) {
        Project project = findByName(name);
        if (project == null) return null;
        return project.getId();
    }

    @Override
    public String getId(final int index) {
        Project project = findByIndex(index);
        if (project == null) return null;
        return project.getId();
    }

}
