package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean isFoundById(final String id) {
        return findById(id) != null;
    }

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public Task update(final String id, final String name, final String description) {
        final Task task = findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        return list.remove(index);
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task updateStatus(final String id, final Status status) {
        final Task task = findById(id);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) task.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task updateProjectId(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : this.list) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, Comparator<Task> comparator) {
        final List<Task> list = findAllByProjectId(projectId);
        list.sort(comparator);
        return list;
    }

    @Override
    public boolean isTaskInProject(final String taskId, final String projectId) {
        return (findAllByProjectId(projectId).contains(findById(taskId)));
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
        return list;
    }

    @Override
    public String getId(final String name) {
        Task task = findByName(name);
        if (task == null) return null;
        return task.getId();
    }

    @Override
    public String getId(final int index) {
        Task task = findByIndex(index);
        if (task == null) return null;
        return task.getId();
    }

}
