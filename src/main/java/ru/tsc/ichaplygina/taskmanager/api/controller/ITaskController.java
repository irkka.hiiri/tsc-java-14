package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.model.Task;

public interface ITaskController {

    void clear();

    void completeById();

    void completeByIndex();

    void completeByName();

    void create();

    void removeById();

    void removeByIndex();

    void removeByName();

    void showTask(Task task);

    void showById();

    void showByIndex();

    void showByName();

    void showList();

    void startById();

    void startByIndex();

    void startByName();

    void updateById();

    void updateByIndex();

}
