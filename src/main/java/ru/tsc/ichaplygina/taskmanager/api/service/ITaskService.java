package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(final String name, final String description);

    void clear();

    Task completeById(final String id);

    Task completeByIndex(final int index);

    Task completeByName(final String name);

    List<Task> findAll(final Comparator<Task> comparator);

    Task findById(final String id);

    Task findByName(final String name);

    Task findByIndex(final int index);

    int getSize();

    boolean isEmpty();

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task startById(final String id);

    Task startByIndex(final int index);

    Task startByName(final String name);

    Task updateByIndex(final int index, final String name, final String description);

    Task updateById(final String id, final String name, final String description);

    Task updateStatus(String id, Status status);

}
