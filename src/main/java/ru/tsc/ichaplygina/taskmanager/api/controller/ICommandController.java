package ru.tsc.ichaplygina.taskmanager.api.controller;

public interface ICommandController {

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

    void showSystemInfo();

    void showVersion();

    void showWelcome();

    void showUnknown(String arg);

    void exit();

}
