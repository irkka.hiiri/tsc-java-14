package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectController {

    void completeById();

    void completeByIndex();

    void completeByName();

    void create();

    void showProject(Project project);

    void showById();

    void showByIndex();

    void showByName();

    void showList();

    void startById();

    void startByIndex();

    void startByName();

    void updateById();

    void updateByIndex();

}
