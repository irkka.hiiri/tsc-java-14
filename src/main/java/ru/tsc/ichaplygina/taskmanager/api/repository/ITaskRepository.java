package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void add(final Task task);

    Task update(String id, String name, String description);

    void clear();

    Task findById(final String id);

    Task findByName(final String name);

    Task findByIndex(final int index);

    String getId(String name);

    String getId(int index);

    int getSize();

    boolean isEmpty();

    boolean isFoundById(String id);

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId, Comparator<Task> comparator);

    boolean isTaskInProject(String taskId, String projectId);

    void remove(final Task task);

    List<Task> removeAllByProjectId(final String projectId);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task updateStatus(String id, Status status);

    Task updateProjectId(final String taskId, final String projectId);

}
