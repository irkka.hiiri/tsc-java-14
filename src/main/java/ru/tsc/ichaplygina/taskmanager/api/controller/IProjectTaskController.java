package ru.tsc.ichaplygina.taskmanager.api.controller;

public interface IProjectTaskController {

    void addTaskToProject();

    void clearAllProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeTaskFromProject();

    void showTaskListByProjectId();

}
