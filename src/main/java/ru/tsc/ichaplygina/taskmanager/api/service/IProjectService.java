package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(final String name, final String description);

    Project completeById(final String id);

    Project completeByIndex(final int index);

    Project completeByName(final String name);

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    boolean isEmpty();

    Project startById(final String id);

    Project startByIndex(final int index);

    Project startByName(final String name);

    Project updateProject(String id, String name, String description);

    Project updateByIndex(final int index, final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    Project updateStatus(String id, Status status);

}
