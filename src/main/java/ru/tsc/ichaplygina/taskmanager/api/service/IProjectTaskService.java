package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    void clearProjects();

    int getProjectSize();

    Task removeTaskFromProject(String projectId, String taskId);

    List<Task> findAllTasksByProjectId(String projectId, final Comparator<Task> taskComparator);

    Project removeProjectById(final String projectId);

    Project removeProjectByIndex(final int projectIndex);

    Project removeProjectByName(final String projectName);

    Task addTaskToProject(String projectId, String taskId);

}
