package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.model.Command;

public interface ICommandService {

    Command[] getAllCommands();

    String[] getTerminalCommands();

    String[] getArguments();

}