package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.model.Command;

public interface ICommandRepository {

    Command[] getAllCommands();

    String[] getTerminalCommands();

    String[] getArguments();

}
