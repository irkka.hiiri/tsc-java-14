package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showNotFound() {
        printLinesWithEmptyLine(TASK_NOT_FOUND);
    }

    private void showRemoveResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_REMOVED);
        else showNotFound();
    }

    private void showUpdateResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_UPDATED);
        else printLinesWithEmptyLine(TASK_UPDATE_ERROR);
    }

    @Override
    public void clear() {
        final int count = taskService.getSize();
        taskService.clear();
        printLinesWithEmptyLine(count + TASKS_CLEARED);
    }

    @Override
    public void create() {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        if (taskService.add(name, description) == null) printLinesWithEmptyLine(TASK_CREATE_ERROR);
        else printLinesWithEmptyLine(TASK_CREATED);
    }

    @Override
    public void showList() {
        if (taskService.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND);
            return;
        }
        Comparator<Task> comparator = readComparator();
        printListWithIndexes(taskService.findAll(comparator));
    }

    @Override
    public void showTask(final Task task) {
        if (task == null) showNotFound();
        else printLinesWithEmptyLine(task);
    }

    @Override
    public void showById() {
        final String id = readLine(ID_INPUT);
        final Task task = taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final Task task = taskService.findByIndex(index - 1);
        showTask(task);
    }

    @Override
    public void removeById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = taskService.removeById(id) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = taskService.removeByName(name) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = taskService.removeByIndex(index - 1) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void showByName() {
        final String name = readLine(NAME_INPUT);
        final Task task = taskService.findByName(name);
        showTask(task);
    }


    @Override
    public void startByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = taskService.startByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = taskService.startById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = taskService.startByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = taskService.completeByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = taskService.completeById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = taskService.completeByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void updateById() {
        final String id = readLine(ID_INPUT);
        if (this.taskService.findById(id) == null) {
            showNotFound();
            return;
        }
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = taskService.updateById(id, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void updateByIndex() {
        final int index = readNumber(INDEX_INPUT);
        if (this.taskService.findByIndex(index - 1) == null) {
            showNotFound();
            return;
        }
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = taskService.updateByIndex(index - 1, name, description) != null;
        showUpdateResult(isSuccess);
    }

}
