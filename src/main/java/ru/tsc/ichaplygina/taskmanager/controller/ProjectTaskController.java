package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.IProjectTaskController;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.TASK_ADD_TO_PROJECT_ERROR;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    private void showRemoveTaskFromProjectResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_REMOVED_FROM_PROJECT);
        else printLinesWithEmptyLine(TASK_REMOVE_FROM_PROJECT_ERROR);
    }

    private void showAddTaskToProjectResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(TASK_ADDED_TO_PROJECT);
        else printLinesWithEmptyLine(TASK_ADD_TO_PROJECT_ERROR);
    }

    private void showRemoveProjectResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(PROJECT_REMOVED);
        else printLinesWithEmptyLine(PROJECT_NOT_FOUND);
    }

    @Override
    public void addTaskToProject() {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        final boolean isSuccess = projectTaskService.addTaskToProject(projectId, taskId) != null;
        showAddTaskToProjectResult(isSuccess);
    }

    @Override
    public void clearAllProjects() {
        final int count = projectTaskService.getProjectSize();
        projectTaskService.clearProjects();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

    @Override
    public void showTaskListByProjectId() {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final Comparator<Task> taskComparator = readComparator();
        final List<Task> list = projectTaskService.findAllTasksByProjectId(projectId, taskComparator);
        if (list == null) {
            printLinesWithEmptyLine(PROJECT_NOT_FOUND);
            return;
        }
        if (list.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND_IN_PROJECT);
            return;
        }
        printListWithIndexes(list);
    }

    @Override
    public void removeTaskFromProject() {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        final boolean isSuccess = projectTaskService.removeTaskFromProject(projectId, taskId) != null;
        showRemoveTaskFromProjectResult(isSuccess);
    }

    @Override
    public void removeProjectById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectById(id) != null;
        showRemoveProjectResult(isSuccess);
    }

    @Override
    public void removeProjectByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectByName(name) != null;
        showRemoveProjectResult(isSuccess);
    }

    @Override
    public void removeProjectByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectByIndex(index - 1) != null;
        showRemoveProjectResult(isSuccess);
    }

}
