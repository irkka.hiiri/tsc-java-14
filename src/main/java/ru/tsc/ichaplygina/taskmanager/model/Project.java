package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.api.entity.IWbs;

public class Project extends AbstractBusinessEntity implements IWbs {

    public Project() {
    }

    public Project(final String name) {
        super(name);
    }

    public Project(final String name, final String description) {
        super(name, description);
    }

}
