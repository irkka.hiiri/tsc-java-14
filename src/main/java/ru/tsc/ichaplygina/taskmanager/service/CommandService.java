package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getAllCommands() {
        return commandRepository.getAllCommands();
    }

    @Override
    public String[] getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public String[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
