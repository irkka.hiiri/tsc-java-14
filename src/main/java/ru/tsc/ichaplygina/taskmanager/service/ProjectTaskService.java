package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.EMPTY;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public int getProjectSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task addTaskToProject(final String projectId, final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) return null;
        if (!taskRepository.isFoundById(taskId) || !projectRepository.isFoundById(projectId)) return null;
        return taskRepository.updateProjectId(taskId, projectId);
    }

    @Override
    public Task removeTaskFromProject(final String projectId, final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) return null;
        if (!taskRepository.isFoundById(taskId) || !projectRepository.isFoundById(projectId)) return null;
        if (!taskRepository.isTaskInProject(taskId, projectId)) return null;
        return taskRepository.updateProjectId(taskId, EMPTY);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final Comparator<Task> taskComparator) {
        if (isEmptyString(projectId)) return null;
        if (taskComparator == null) return taskRepository.findAllByProjectId(projectId);
        return taskRepository.findAllByProjectId(projectId, taskComparator);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (isEmptyString(projectId)) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeProjectByIndex(final int projectIndex) {
        if (isInvalidListIndex(projectIndex, projectRepository.getSize())) return null;
        final String projectId = projectRepository.getId(projectIndex);
        return removeProjectById(projectId);
    }

    @Override
    public Project removeProjectByName(final String projectName) {
        if (isEmptyString(projectName)) return null;
        final String projectId = projectRepository.getId(projectName);
        return removeProjectById(projectId);
    }

    @Override
    public void clearProjects() {
        for (final Project project : projectRepository.findAll())
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clear();
    }

}
