package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public boolean isEmpty() {
        return taskRepository.isEmpty();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return taskRepository.findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmptyString(name)) return null;
        final Task task = new Task(name, description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (isEmptyString(id)) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (isEmptyString(name)) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (isEmptyString(id)) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (isEmptyString(name)) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (isEmptyString(id) || isEmptyString(name)) return null;
        if (!taskRepository.isFoundById(id)) return null;
        return taskRepository.update(id, name, description);
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        final String id = taskRepository.getId(index);
        if (!taskRepository.isFoundById(id)) return null;
        return updateById(id, name, description);
    }

    @Override
    public Task updateStatus(final String id, final Status status) {
        if (isEmptyString(id) || status == null) return null;
        if (!taskRepository.isFoundById(id)) return null;
        return taskRepository.updateStatus(id, status);
    }

    @Override
    public Task startById(final String id) {
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task startByIndex(int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        final String id = taskRepository.getId(index);
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task startByName(final String name) {
        if (isEmptyString(name)) return null;
        final String id = taskRepository.getId(name);
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task completeById(final String id) {
        if (isEmptyString(id)) return null;
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Task completeByIndex(int index) {
        if (isInvalidListIndex(index, taskRepository.getSize())) return null;
        final String id = taskRepository.getId(index);
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Task completeByName(final String name) {
        if (isEmptyString(name)) return null;
        final String id = taskRepository.getId(name);
        return updateStatus(id, Status.COMPLETED);
    }

}
